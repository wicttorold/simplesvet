/**
 * Menu lateral
 */
$(document).ready(function() {

    documentW = $(document).width();
    if ( documentW >= 991){
        wSize = "-40vw";
    }
    else {
        wSize = "-100vw";
    }

    var sideMenu = false;
     $(".menucall").click(function() {
        if (!sideMenu) {
            abrirMenu();
            sideMenu = true;
            mainClicavel(true);
        }
        else {
            sideMenu = false;
            fecharMenu();  
            mainClicavel(false);
        }
     });
    function abrirMenu() {
        $("nav").animate({right: "0vw"});
        $("main").animate({left: wSize});
        $("main").css({opacity: "0.3"});
        $("#topbar").css({opacity: "0.3"});
        $("header").css({opacity: "0.3"});
        $("#shadowmenu").css("z-index","99");
    }
    function fecharMenu() {
        $("nav").animate({right: wSize});
        $("main").animate({left: "0vw"});
        $("main").css("opacity","1");
        $("#topbar").css({opacity: "1"});
        $("header").css({opacity: "1"});
        $("#shadowmenu").css("z-index","0");
        $(".menucall").css({marginRight: "0"});
    }
    function mainClicavel(clicavel) {
        if(clicavel == true) {
            $("#shadowmenu").click(function() {
                fecharMenu(); 
                sideMenu = false; 
                clicavel = false;                
            });
        }   
    }      
 }); 


function loadUrl(newLocation)
{
window.location = newLocation;
return false;
}

$("div.carousel-inner div.carousel-item:first").addClass("active");

jQuery(function ($) {
    var $active = $('#accordion1 .panel-collapse.in').prev().addClass('active');
    $active.find('a').append('<span class="glyphicon glyphicon-minus pull-right"></span>');
    $('#accordion1 .panel-heading').not($active).find('a').prepend('<span class="glyphicon glyphicon-plus pull-right"></span>');
    $('#accordion1').on('show.bs.collapse', function (e) {
        $('#accordion1 .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    });
    $('#accordion1').on('hide.bs.collapse', function (e) {
        $(e.target).prev().removeClass('active').find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    });

    var $active2 = $('#accordion2 .panel-collapse.in').prev().addClass('active');
    $active2.find('a').append('<span class="glyphicon glyphicon-minus pull-right"></span>');
    $('#accordion2 .panel-heading').not($active2).find('a').prepend('<span class="glyphicon glyphicon-plus pull-right"></span>');
    $('#accordion2').on('show.bs.collapse', function (e) {
        $('#accordion2 .panel-heading.active').removeClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        $(e.target).prev().addClass('active').find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
    });
    $('#accordion2').on('hide.bs.collapse', function (e) {
        $(e.target).prev().removeClass('active').find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    });

});


$('.btn-experimente').click(function() {        
    var dlink = $(this).data("link");
    console.log(dlink);
});