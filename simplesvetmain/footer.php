<?php echo get_template_part('components/experimente');?>
<footer class="float-left w-100">
    <div class="container pt-5 pb-3">
        <div class="row text-center text-md-left">
            <div class="col-6 offset-6 mb-3 col-md-5 offset-md-7 float-right pb-3  d-none d-md-block">
                    <?php echo get_template_part('components/sevenDots'); ?>
            </div>
            <div class="w-100 mb-1 float-left"></div>
            
            <div class="col-8 offset-2 col-md-4 offset-md-0 col-lg-3 menu">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet-gestao.png" alt="SimplesVet" class="logo mb-4" />
                <div class="col-12 text-center mb-5 fa-social d-md-none d-md-none p-0">
                    <a alt="Link para o Facebook da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Facebook'); ?>"><i class="fab fa-facebook-f fa-2x"></i></a>
                    <a alt="Link para o Instagram da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Instagram'); ?>"><i class="fab fa-instagram ml-4 fa-2x"></i></a>
                    <a alt="Link para o Youtube da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Youtube'); ?>"><i class="fab fa-youtube ml-4 fa-2x"></i></a>
                    <a alt="Link para o Linkedin da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Linkedin'); ?>"><i class="fab fa-linkedin-in ml-4 fa-2x"></i></a>
                </div>
                <div class="col-12 text-center mb-5 fa-social d-none d-md-block">
                    <a alt="Link para o Facebook da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Facebook'); ?>"><i class="fab fa-facebook-f fa-lg"></i></a>
                    <a alt="Link para o Instagram da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Instagram'); ?>"><i class="fab fa-instagram ml-4 fa-lg"></i></a>
                    <a alt="Link para o Youtube da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Youtube'); ?>"><i class="fab fa-youtube ml-4 fa-lg"></i></a>
                    <a alt="Link para o Linkedin da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Linkedin'); ?>"><i class="fab fa-linkedin-in ml-4 fa-lg"></i></a>
                </div>                
            </div>

            <div class="col-12 col-md-8 offset-md-0 offset-lg-1">
                <div class="row">
                    <div class="col-12 col-md-4 menu text-center text-md-left">

                        <?php 
                        $menu_ID = 4;
                        $nav_menu = wp_get_nav_menu_object( $menu_ID );
                        wp_nav_menu( array(
                        'theme_location'  => 'footer_link1',
                        'items_wrap'      => '<p class="h3 mt-3 mt-md-0">'.$nav_menu->name.'</p><ul class="list-unstyled mb-0">%3$s</ul>'
                        )
                        );
                        ?>

                    </div>
                    <div class="col-12 col-md-4 menu text-center text-md-left">

                        <?php 
                        $menu_ID = 2;
                        $nav_menu = wp_get_nav_menu_object( $menu_ID );
                        wp_nav_menu( array(
                        'theme_location'  => 'footer_link2',
                        'items_wrap'      => '<p class="h3 mt-3 mt-md-0">'.$nav_menu->name.'</p><ul class="list-unstyled mb-0">%3$s</ul>'
                        )
                        );
                        ?>

                    </div>
                    <div class="col-12 col-md-4 menu text-center text-md-left">

                        <?php 
                        $menu_ID = 3;
                        $nav_menu = wp_get_nav_menu_object( $menu_ID );
                        wp_nav_menu( array(
                        'theme_location'  => 'footer_contacts',
                        'items_wrap'      => '<p class="h3 mt-3 mt-md-0">'.$nav_menu->name.'</p><ul class="list-unstyled mb-0">%3$s</ul>'
                        )
                        );
                        ?>

                    </div>  
                </div>
            </div>           
        </div>
    </div>
    <div id="vetlovers">
        <div class="container">
            <div class="row p-3 p-md-1">
                <div class="col-12 col-sm-8">
                    <p class="my-4 p-0"><?php echo get_theme_mod('svCopyright_textarea'); ?></p>
                </div>
                <div class="col-4 offset-4 text-center p-0 col-sm-2 offset-sm-2 col-md-1 offset-md-3 text-sm-right p-sm-1">
                    <p class="p-0 m-0 pt-2">Somos</p>
                    <img class="mb-4 mb-md-0" src="<?php echo get_template_directory_uri(); ?>/img/vetlovers.png" alt="vetlovers">
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/parallax.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/page_scroll_indicator.js"></script>



<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<script type="text/javascript">
AOS.init({
  // Global settings
  disable: 'mobile', // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  offset: 120, // offset (in px) from the original trigger point
  once: true, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
});
</script>


<?php wp_footer(); ?>

<script>
    $(document).ready(function() {
        var wwus = document.getElementById('workwithus');
        if (wwus === null) {}
        else { $(".quemsomos").css('display','none')} ;
    });
</script>