<?php get_header(); ?>
<main class="position-relative">
    <?php echo get_template_part('components/topMenu'); ?>
    <section id="single" class="container-fluid">
        <article id="articlebox" class="row">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>
            <div id="post" class="w-100" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>') no-repeat center center; background-size: cover">  
                <div class="mask py-4 position-relative align-content-end text-center row m-0">
                    <div class="d-block col-10 col-md-8 col-xl-5 position-relative mx-auto p-0">
                        <h1 class="m-0 d-none"><?php the_field('personagem_local'); ?></h1>
                        <p class="h2 w-100 title"><?php the_title(''); ?></p>
                    </div>
                </div>
            </div>
            <div class="pt-4 w-100">            
                <?php get_sidebar(''); ?>
                <?php echo get_template_part('components/acfEditor'); ?>
            </div>
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>           
        </article>
        <?php echo get_template_part('components/nextnprev'); ?>
    </section>
</main>
<?php get_footer(''); ?>

<script>
// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};
function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/readingTime.min.js"></script>
<script type="text/javascript">
$(function() {

    $('article').readingTime({
    wordCountTarget: 'eta',
    lang: 'pt',
    wordsPerMinute: '190',
    });

});
</script>
<script type="text/javascript">
$(document).ready(function() {
if ( documentW >= 991){

    $('div.bgParallax').each(function(){
    var $obj = $(this);
    $(window).scroll(function() {
        var yPos = -($(window).scrollTop() / $obj.data('speed') +200) ; 
        console.log(yPos);
        var bgpos = 'center '+ yPos + 'px';
        $obj.css('background-position', bgpos );
    });
    
});

}
else {
}
});
</script>