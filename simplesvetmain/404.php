<?php get_header(); ?>
<main class="position-relative"> 
    <?php echo get_template_part('components/topMenu'); ?>
    <article class="container-fluid">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="post mb-5 row">  
            <div class="mask w-100 m-0 pb-3 row align-items-end">
                <div class="postInfo text-center col py-3 p-0 text-white">
                    <h2><?php the_title(''); ?></h2>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3">
            <p class="display-4">404</p>
        </div>             
        <?php endwhile; endif; wp_reset_postdata(); ?>
        <div class="row justify-content-center py-5">
            <?php echo get_template_part("components/fiveDots"); ?>
        </div>            
    </article>   
</main>
<?php get_footer(''); ?>