<?php

// Theme Functions
add_theme_support( 'post-thumbnails' );
add_image_size( 'facebook', 484, 252, true );

/* Thumbnail */
function get_thumb_share( $post_id = 0 ) {
	$post_id = (int) $post_id;
	if ( !$post = get_post( $post_id ) )
		return false;
	if ( !$url = wp_get_attachment_url( $post->ID ) )
		return false;
	$sized = image_downsize( $post_id, 'facebook' );
	if ( $sized )
		return $sized[0];
	if ( !$thumb = wp_get_attachment_thumb_file( $post->ID ) )
		return false;
	$url = str_replace(basename($url), basename($thumb), $url);
	return apply_filters( 'wp_get_attachment_thumb_url', $url, $post->ID );
}

function get_thumb_full( $post_id = 0 ) {
	$post_id = (int) $post_id;
	if ( !$post = get_post( $post_id ) )
		return false;
	if ( !$url = wp_get_attachment_url( $post->ID ) )
		return false;
	$sized = image_downsize( $post_id, 'full' );
	if ( $sized )
		return $sized[0];
	if ( !$thumb = wp_get_attachment_thumb_file( $post->ID ) )
		return false;
	$url = str_replace(basename($url), basename($thumb), $url);
	return apply_filters( 'wp_get_attachment_thumb_url', $url, $post->ID );
}

/* Menu */
register_nav_menu( 'footer_contacts', 'Contatos - Rodapé' );
register_nav_menu( 'footer_link1', 'Links 1 - Rodapé' );
register_nav_menu( 'footer_link2', 'Links 2 - Rodapé' );
register_nav_menu( 'navMenu_side', 'NAV Menu' );


function default_attachment_display_settings() {
	update_option( 'image_default_align', 'center' );
	update_option( 'image_default_link_type', 'none' );
	update_option( 'image_default_size', 'large' );
}


/**
 * Função para registrar as opções do customizer
 */
function copyrightSV( $wp_customize ) {
    $wp_customize->add_section(
        'svCopyright',
        array(
            'title'       => 'Copyright (Rodapé)',
            'description' => 'Mensagem de copyright inserida no rodapé do site',
            'priority'    => 91,
        )
    );
    // svCopyright_textarea
    $wp_customize->add_setting(
        'svCopyright_textarea', // svCopyright_textarea
        array(
            'default'   => '', // Valor padrão 
            'transport' => 'refresh', // Transport
        )
    );
    // Controle do svCopyright_textarea
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        "svCopyright_textarea", // svCopyright_textarea
        array(
            "label"    => "Link para ", // Rótulo do campo
            "section"  => "svCopyright", // A seção
            "settings" => "svCopyright_textarea", // Settings do campo
            "type"     => "textarea", // Input do tipo "textarea"
        )
    ));

}
// Utiliza o gancho para adicionar nossa função
add_action( 'customize_register', 'copyrightSV' );

function redessociaisSV( $wp_customize ) {
    $wp_customize->add_section(
        'svRedesSociais',
        array(
            'title'       => 'Redes Sociais',
            'description' => 'Mensagem de copyright inserida no rodapé do site',
            'priority'    => 90,
        )
    );
    // svRedes_Facebook
    $wp_customize->add_setting(
        'svRedes_Facebook', // svRedes_Facebook
        array(
            'default'   => '', // Valor padrão 
            'transport' => 'refresh', // Transport
        )
    );
    // Controle do svRedes_Facebook
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        "svRedes_Facebook", // svRedes_Facebook
        array(
            "label"    => "Link para Facebook", // Rótulo do campo
            "section"  => "svRedesSociais", // A seção
            "settings" => "svRedes_Facebook", // Settings do campo
            "type"     => "text", // Input do tipo "text"
        )
    ));
    $wp_customize->add_setting(
        'svRedes_Instagram', // svRedes_Instagram
        array(
            'default'   => '', // Valor padrão 
            'transport' => 'refresh', // Transport
        )
    );    
    // Controle do svRedes_Instagram
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        "svRedes_Instagram", // svRedes_Instagram
        array(
            "label"    => "Link para Instagram", // Rótulo do campo
            "section"  => "svRedesSociais", // A seção
            "settings" => "svRedes_Instagram", // Settings do campo
            "type"     => "text", // Input do tipo "text"
        )
    ));
    $wp_customize->add_setting(
        'svRedes_Youtube', // svRedes_Youtube
        array(
            'default'   => '', // Valor padrão 
            'transport' => 'refresh', // Transport
        )
    );
    // Controle do svRedes_Youtube
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        "svRedes_Youtube", // svRedes_Youtube
        array(
            "label"    => "Link para Youtube", // Rótulo do campo
            "section"  => "svRedesSociais", // A seção
            "settings" => "svRedes_Youtube", // Settings do campo
            "type"     => "text", // Input do tipo "text"
        )
    ));
    $wp_customize->add_setting(
        'svRedes_Linkedin', // svRedes_Linkedin
        array(
            'default'   => '', // Valor padrão 
            'transport' => 'refresh', // Transport
        )
    );
    // Controle do svRedes_Linkedin
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        "svRedes_Linkedin", // svRedes_Linkedin
        array(
            "label"    => "Link LinkedIn", // Rótulo do campo
            "section"  => "svRedesSociais", // A seção
            "settings" => "svRedes_Linkedin", // Settings do campo
            "type"     => "text", // Input do tipo "text"
        )
    ));
}
// Utiliza o gancho para adicionar nossa função
add_action( 'customize_register', 'redessociaisSV' );

function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true ) {
	if ( null === $wp_query ) {
		global $wp_query;
	}
	$pages = paginate_links( [
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'format'       => '?paged=%#%',
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'total'        => $wp_query->max_num_pages,
			'type'         => 'array',
			'show_all'     => false,
			'end_size'     => 3,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => __( '« Anterior' ),
			'next_text'    => __( 'Próxima »' ),
			'add_args'     => false,
			'add_fragment' => ''
		]
	);
	if ( is_array( $pages ) ) {
		//$paged = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );
		$pagination = '<div class="w-100 text-center mx-auto"><ul class="pagination my-5 w-100 d-flex justify-content-center">';
		foreach ( $pages as $page ) {
			$pagination .= '<li class="page-item '.(strpos($page, 'current') !== false ? 'active' : '').'"> ' . str_replace( 'page-numbers', 'page-link', $page ) . '</li>';
		}
        $pagination .= '</ul></div>';
        
		if ( $echo ) {
			echo $pagination;
		} else {
			return $pagination;
		}
	}
	return null;
}