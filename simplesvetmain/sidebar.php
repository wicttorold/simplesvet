<aside class='col-12 col-md-2 col-xl-3 mt-1 float-right affix' data-aos="fade-down" data-aos-duration="1000" data-aos-offset="300">
    <p class='col-6 col-md-12 float-left p-0'>Tempo de Leitura:<br /><span class="eta"></span></p>
    <p class='col-6 col-md-12 float-left p-0'>Publicado em:<br /><?php the_date(''); ?></p>
</aside>