<?php
// check if the flexible content field has rows of data
if( have_rows('editor') ): ?>
    <?php
    while ( have_rows('editor') ) : the_row(); ?>
        <?php if( get_row_layout() == 'texto_simples' ): ?>
            <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3 texto_simples float-left">
                <?php the_sub_field('texto'); ?>
            </div>            
        <?php elseif( get_row_layout() == 'intertitulo' ): ?>
            <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3 float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                <h3 class="intertitulo color blue"><?php the_sub_field('texto'); ?></h3>
            </div>   
        <?php elseif( get_row_layout() == 'destaque-col-8' ): ?>
            <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3 paragrafoDestacado destaque-col-8 float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                <blockquote><?php the_sub_field('texto'); ?></blockquote>
            </div>                
        <?php elseif( get_row_layout() == 'destaque-col-12' ): ?>
            <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 paragrafoDestacado destaque-col-12 float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                <?php the_sub_field('texto'); ?>
            </div>
        <?php elseif( get_row_layout() == 'destaque-texto' ): ?>
        <div class="paragraph_image mb-4 container-fluid float-left destaque-texto float-left">
            <div class="row align-items-center">             
                <div class="col-12 col-md-4 offset-md-2 col-xl-3 offset-xl-3 paragrafoDestacado"  data-aos="zoom-in" data-aos-duration="1500" data-aos-offset="50">  
                    <?php the_sub_field('citacao'); ?>
                </div>      
                <div class="col-12 col-md-4 col-xl-3 float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                    <?php the_sub_field('texto'); ?>
                </div>
            </div>
        </div>
        <?php elseif( get_row_layout() == 'imagem-coluna' ): ?>
            <div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3 mb-4 float-left" data-aos="zoom-in" data-aos-duration="1500" data-aos-offset="50">
                <img class="rounded" src="<?php the_sub_field('imagemfull'); ?>"/ alt="imagem" style="width:100%;">
            </div>                  
        <?php elseif( get_row_layout() == 'imagem-full' ): ?>              
            <div class="w-100 mb-4 float-left bgParallax" data-speed="20" style="background:url('<?php the_sub_field('imagemfull'); ?>') right center no-repeat; background-size: cover" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                &nbsp;
            </div>                
        <?php elseif( get_row_layout() == 'texto-imagem-direita' ): ?>
            <div class="paragraph_image mb-4 container-fluid float-left">
                <div class="row"> 
                    <div class="col-12 col-md-4 offset-md-2 col-xl-3 offset-xl-3 mb-4 mb-md-0" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                        <?php the_sub_field('texto'); ?>
                    </div>
                    <div class="col-12 col-md-4 col-xl-4" data-aos="zoom-in-right" data-aos-duration="1500" data-aos-offset="50">
                        <div class="rounded imgBG w-100" style="background:url('<?php the_sub_field('imagem'); ?>') right center no-repeat; background-size:cover; height:100%;">
                            &nbsp;
                        </div>
                    </div>        
                </div>
            </div>
        <?php elseif( get_row_layout() == 'paragraph_imageRight_full' ): ?>
            <div class="paragraph_image mb-4 container-fluid float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                <div class="row"> 
                    <div class="col-12 col-md-4 offset-md-2 col-xl-3 offset-xl-3  mb-4 mb-md-0">
                        <?php the_sub_field('texto'); ?>
                    </div>
                    <div class="col-12 col-md-4 col-xl-6 pr-0">
                        <div class="rounded-left imgBG w-100" style="background:url('<?php the_sub_field('imagem'); ?>') right center no-repeat; background-size:cover; height:100%;">
                            &nbsp;
                        </div>
                    </div>        
                </div>
            </div>

        <?php elseif( get_row_layout() == 'texto-imagem-esquerda' ): ?>
            <div class="paragraph_image mb-4 container-fluid float-left">
                <div class="row"> 
                    <div class="col-12 offset-0 col-md-5 offset-md-1 col-xl-4 offset-xl-2 mb-4 mb-md-0" data-aos="zoom-in-left" data-aos-duration="1500" data-aos-offset="50">
                        <div class="rounded imgBG w-100" style="background:url('<?php the_sub_field('imagem'); ?>') right center no-repeat; background-size:cover; height:100%;">
                            &nbsp;
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-xl-3 offset-xl-0" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                        <?php the_sub_field('texto'); ?>
                    </div>                        
                </div>
            </div> 
        <?php elseif( get_row_layout() == 'paragraph_imageLeft_full' ): ?>
            <div class="paragraph_image mb-4 container-fluid float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                <div class="row"> 
                    <div class="col-12 offset-0 col-md-6 col-xl-6 pl-0 mb-4 mb-md-0">
                        <div class="rounded-right imgBG w-100" style="background:url('<?php the_sub_field('imagem'); ?>') right center no-repeat; background-size:cover; height:100%;">
                            &nbsp;
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-xl-3 offset-xl-0">
                        <?php the_sub_field('texto'); ?>
                    </div>                        
                </div>
            </div>
        <?php  elseif( get_row_layout() == 'imagens-50-50' ): ?>
            <div class="paragraph_image mb-4 container-fluid float-left">
                <div class="row align-middle"> 
                    <div class="col-10 offset-1 col-md-4 offset-md-2 col-xl-4 offset-xl-2 mb-4 mb-md-0" data-aos="zoom-in" data-aos-duration="900" data-aos-offset="50">
                        <img class="rounded" src="<?php the_sub_field('imagem'); ?>"/ alt="imagem" style="width:100%;">
                    </div>         
                    <div class="col-10 offset-1 col-md-4 offset-md-0 col-xl-4" data-aos="zoom-in" data-aos-duration="1300" data-aos-offset="50">
                        <img class="rounded" src="<?php the_sub_field('imagem_2'); ?>"/ alt="imagem" style="width:100%;">
                    </div>  
                </div> 
            </div>
        <?php elseif( get_row_layout() == 'site' ): ?>
            <div class="col-12 col-md-10 offset-md-1 float-left mb-3 text-center">
                <hr class="float-left w-100 mb-4 mb-md-5" />
                    <div class="col-10 offset-1 offset-md-0 col-md-8 col-xl-6 offset-xl-1 float-left mb-4 mb-md-0 text-center">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-completo-h.svg" class="d-none d-md-block" alt="Simplesvet - Gestão Pet suuuper simples" />
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet-gestao.png" class="d-block d-md-none" alt="Simplesvet - Gestão Pet suuuper simples" />
                    </div>
                    <div class="col-12 col-md-4 col-xl-4 float-left">
                        <a href="//simples.vet" target="_blank" class="btn-sm btn bg purple text-white px-4 mt-2 mr-auto">Conheça</a>
                    </div>
                <hr class="float-left w-100 mt-4 mt-md-5" />
            </div>   
        <?php elseif( get_row_layout() == 'lead' ): ?>
            <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 float-left text-center">
                <div class="lead rounded mb-3 py-3">
                    <p class="col-12 col-md-5 float-left mb-2 mb-md-0">
                        Receba dicas GRATUITAS sobre gestão para pet shops e clínicas veterinárias!
                    </p>
                    <div class="mx-0 px-0 col-12 col-md-7 row pt-1">
                        <div class="col-12 col-md-9 mb-3 mb-md-0">
                            <input type="email" class="form-control float-left" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Informe seu e-mail" />
                        </div>
                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                            <button type="submit" class="btn btn-primary bg blue text-white float-right w-100 btn-sm">
                                Enviar
                            </button>
                        </div>
                    </div>    
                </div>                
            </div>
        <?php elseif( get_row_layout() == 'experimentacao' ): ?>
            <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 float-left mb-3 text-center">
                <hr />
                <div class="row align-items-center py-4">
                    <div class="col-12 col-md-6">
                        <p class="h6 color turquoise"><?php the_sub_field('titulo'); ?></p>
                        <p><?php the_sub_field('texto'); ?></p>
                        <button type="submit" class="btn btn-primary bg blue text-white btn-sm mb-3 mb-md-0">
                            Experimente Grátis
                        </button>                            
                    </div>
                    <div class="col-12 col-md-6">
                        <img class="rounded w-100" src="<?php the_sub_field('imagem'); ?>" alt="Esperimente grátis" />
                    </div>                        
                </div>
                <hr />
            </div>
        <?php else: ?>
            <?php the_content(''); ?>
        <?php endif; ?>
    <?php endwhile; else : ?>
<?php
endif;
?>
<div class="w-100 float-left">
    <?php the_content(''); ?>
</div>