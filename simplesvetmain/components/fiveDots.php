<div class="fivedots justify-content-around">
    <div class="bg blue animate" data-aos="fade-up" data-aos-duration="300"></div>
    <div class="bg turquoise animate" data-aos="fade-up" data-aos-duration="500"></div>
    <div class="bg yellow animate" data-aos="fade-up" data-aos-duration="700"></div>
    <div class="bg red animate" data-aos="fade-up" data-aos-duration="900"></div>
    <div class="bg purple animate" data-aos="fade-up" data-aos-duration="1100"></div>
</div>