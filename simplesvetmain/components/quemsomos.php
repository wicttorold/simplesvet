<div class="container my-2 quemsomos">
    <div class="row">
        <div class="col-10 offset-1 offset-md-0 mb-2 mb-md-0 col-md-6 p-0 float-left rounded" style="z-index:2; background:url('<?php echo get_template_directory_uri(); ?>/img/equipe.png') center center no-repeat; background-size:cover;" id="equipeImg">
        </div>
        <div class="col-10 offset-1 offset-md-0 col-md-7 p-0 py-5 text-white offset-md-5 bg turquoise text-white rounded" style="z-index:1" id="quemsomos">
            <div class="col-12 col-md-8 offset-md-2 px-3">
                <p class="h6">Quem somos</p>
                <p class="h5">Usamos a tecnologia para simplificar a gestão pet e trazer mais felicidade para as pessoas</p>
                <p class="p-0 mb-0">Visite nosso site de cultura e conheça melhor quem faz Simplesvet.</p>
            </div>
        </div>            
    </div>    
</div>