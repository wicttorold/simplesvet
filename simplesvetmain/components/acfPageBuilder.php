<?php
// check if the flexible content field has rows of data
if( have_rows('page_builder') ): ?>
    <?php
    while ( have_rows('page_builder') ) : the_row(); ?>
        <?php if( get_row_layout() == 'texto_simples' ): ?>
            <div class="col-12 col-md-10 offset-1 texto_simples float-left">
                <?php the_sub_field('texto'); ?>
            </div>            
        <?php elseif( get_row_layout() == 'precos' ): ?>
            <div class="col-12 col-md-10 offset-1 float-left mb-3 text-center row justify-content-center">
                <div class="row w-100 p-0 m-0 justify-content-md-center">                
                    <?php if(have_rows('preco')): ?>
                        <?php while(has_sub_field('preco')): ?>                 
                            <div class="col-md-2 mb-3">
                                <div class="w-100 priceBox bg <?php the_sub_field('cor'); ?> p-2 text-white rounded">
                                    <p><?php the_sub_field('usuarios'); ?></p>
                                    <p class="preco">R$ <span><?php the_sub_field('valor'); ?></span></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>                
                <button class="btn bg blue mt-5 text-white rounded-extra m-0 px-5">Iniciar teste grátis</button>                     
            </div> 
        <?php elseif( get_row_layout() == 'features' ): ?>
            <div class="col-12 col-md-10 offset-1 float-left mb-3 mt-4">
                <p class="h3 my-3 w-100"><?php the_sub_field('chamada'); ?></p>
            </div>
            <div class="w-100 float-left mb-4 p-0 row">
                <?php if(have_rows('feature')): ?>
                <?php while(has_sub_field('feature')): ?>                 
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="w-100 bg rounded mx-2">
                            <p class="h4 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('titulo'); ?></p>
                            <p><?php the_sub_field('texto'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>                   
            </div> 
        <?php elseif( get_row_layout() == 'featuresplus' ): ?>
            <div class="col-12 col-md-10 offset-1 float-left mb-3 mt-4">
                <p class="h3 my-3 w-100">
                    <?php the_sub_field('chamada'); ?>
                </p>
            </div>
            <div class="w-100 float-left mb-4 p-0">
                <?php if(have_rows('feature')): ?>
                <?php while(has_sub_field('feature')): ?>                 
                    <div class="col-12 col-md-4 float-left">
                        <div class="mb-3 w-100 text-white p-1 px-2 float-left rounded bg <?php the_sub_field('cor'); ?>">
                            <p class="col-9 p-0 m-0 float-left"><?php the_sub_field('titulo'); ?></p>
                            <p class="col-3 p-0 m-0 text-right float-left">R$ <?php the_sub_field('valor'); ?></p>
                        </div>
                        <div class="featplus">
                            <p><?php the_sub_field('texto'); ?></p>
                            <p><span><?php the_sub_field('nota'); ?></span></p>
                            <a href="<?php the_sub_field('link'); ?>" class="float-left color <?php the_sub_field('cor'); ?>"><p>Saiba mais</p></a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>                   
            </div>
        <?php elseif( get_row_layout() == 'faq' ): ?>
            <div class="col-12 float-left mb-3 mt-4 bg-none row">
                <p class="h3 my-3 w-100">
                    <?php the_sub_field('nome'); ?>
                </p>

                <div class="col-12 col-md-6 p-0">
                    <div class="accordion" id="accordionFAQ">
                        <?php
                        $perNumber = 0;
                        if(have_rows('perguntas')): ?>
                            <?php while(has_sub_field('perguntas')): ?>                          
                                <div class="card">
                                    <div class="card-header" id="heading<?php echo $perNumber; ?>">
                                        <h5 class="float-left" data-toggle="collapse" data-target="#collapse<?php echo $perNumber; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <?php the_sub_field('pergunta'); ?>
                                        </h5>
                                    </div>
                                    <div id="collapse<?php echo $perNumber; ?>" class="collapse <?php if($perNumber == 0) { echo 'show'; }; ?>" aria-labelledby="heading<?php echo $perNumber; ?>" data-parent="#accordionFAQ">
                                        <div class="card-body">
                                            <?php the_sub_field('resposta'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php $perNumber++;
                         endwhile; ?>
                        <?php endif; ?>                          
                    </div>                
                </div> 
                <div class="col-12 col-md-6">
                    <img src="<?php the_sub_field('imagem'); ?>" />
                </div>                                        
            </div>                       
        <?php elseif( get_row_layout() == 'site' ): ?>
            <div class="col-12 col-md-10 offset-md-1 float-left mb-3 text-center row">
                <hr class="float-left w-100 mb-4" />
                    <div class="col-12 col-md-6 offset-md-2 float-left mb-3 mb-md-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-completo-h.svg" alt="Simplesvet - Gestão Pet suuuper simples" />
                    </div>
                    <div class="col-12 col-md-4 float-left">
                        <a href="//simples.vet" target="_blank" class="btn bg purple text-white">Conheça</a>
                    </div>
                <hr class="float-left w-100 mt-4" />
            </div>   
        <?php elseif( get_row_layout() == 'lead' ): ?>
            <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 float-left mb-3 py-3 text-center lead rounded">
                <p class="col-12 col-md-5 float-left mb-0">
                    Receba dicas GRATUITAS sobre gestão para pet shops e clínicas veterinárias!
                </p>
                <div class="mx-0 px-0 col-12 col-md-7 row pt-1">
                    <div class="col-12 col-md-9 mb-3 mb-md-0">
                        <input type="email" class="form-control float-left" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Informe seu e-mail" />
                    </div>
                    <div class="col-12 col-md-3 mb-3 mb-md-0">
                        <button type="submit" class="btn btn-primary bg blue text-white float-right w-100 btn-sm">
                            Enviar
                        </button>
                    </div>
                </div>                    
            </div>
        <?php elseif( get_row_layout() == 'experimentacao' ): ?>
            <div class="col-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 float-left mb-3 text-center">
                <hr />
                <div class="row align-items-center">
                    <div class="col-6">
                        <p class="h6 color turquoise"><?php the_sub_field('titulo'); ?></p>
                        <p><?php the_sub_field('texto'); ?></p>
                        <button type="submit" class="btn btn-primary bg blue text-white btn-sm">
                            Experimente Grátis
                        </button>                            
                    </div>
                    <div class="col-6">
                        <img class="rounded w-100" src="<?php the_sub_field('imagem'); ?>" alt="Esperimente grátis" />
                    </div>                        
                </div>
                <hr />
            </div>
        <?php elseif( get_row_layout() == 'divider' ): ?>            
            <div class="col-12 col-md-10 offset-1 mb-5 divider">
                <div class="w-100 rounded bg <?php the_sub_field('cor'); ?> py-4 text-center text-white">
                    <?php if(get_sub_field('rotulo')) { ?>
                        <span class="bg dark <?php the_sub_field('cor'); ?> rounded-extra px-3 py-1"><?php the_sub_field('rotulo'); ?></span>
                    <?php }  ?>
                    <p class="m-0"><?php the_sub_field('texto'); ?></p>
                    <div class="row w-75 mx-auto">
                        <?php if(have_rows('tags')): ?>
                        <?php while(has_sub_field('tags')): ?>                 
                            <div class="tag mx-auto my-0 d-flex justify-content-center">#<?php the_sub_field('tag'); ?></div>
                        <?php endwhile; ?>
                        <?php endif; ?>   
                    </div> 
                </div>               
            </div>
        <?php elseif( get_row_layout() == 'action' ): ?>  
            <div class="col-12 float-left text-center mb-5">
                <a class="text-white mx-auto btn bg <?php the_sub_field('cor'); ?>" href="<?php the_sub_field('link'); ?>">
                    <?php the_sub_field('texto'); ?>
                </a>
            </div>  
        <?php elseif( get_row_layout() == 'workwithus' ): ?>  
            <div class="col-12 col-md-10 offset-1 my-2">
                <div class="col-10 offset-1 offset-md-0 mb-2 mb-md-0 col-md-6 p-0 float-left rounded" style="z-index:2; background:url('<?php echo get_template_directory_uri(); ?>/img/equipe.png') center center no-repeat; background-size:cover;" id="equipeImgWUS">
                </div>
                <div class="col-10 offset-1 offset-md-0 col-md-7 p-0 py-4 text-white offset-md-5 bg purple text-white rounded mb-5" style="z-index:1" id="workwithus">
                    <div class="col-12 col-md-8 offset-md-2 px-3">
                        <p class="h5"><?php the_sub_field('chamada'); ?></p>
                        <p class="h6"><?php the_sub_field('texto'); ?></p>
                        <button class="btn bg purple dark text-white w-75 mx-auto d-block my-4"><?php the_sub_field('texto_do_link'); ?></button>
                    </div>
                </div>            
            </div>
        <?php elseif( get_row_layout() == 'col-4-content' ): ?>
        <div class="col-12 col-md-10 offset-1 px-0 float-left mb-3 row col-4-content">
            <div class="row w-100 p-0 m-0">                
                <?php if(have_rows('content')): ?>
                    <?php while(has_sub_field('content')): ?>                 
                        <div class="col-12 col-sm-6 col-xl-4 mb-3">
                            <div class="w-100 tag"><?php the_sub_field('tag'); ?></div>
                            <div class="w-100 bg <?php the_sub_field('cor'); ?> p-0 pt-1 text-white rounded">
                                <img class="rounded-bottom w-100" src="<?php the_sub_field('imagem'); ?>" alt="Esperimente grátis" />
                            </div>
                            <p class="h5 my-3 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('titulo'); ?></p>
                            <?php the_sub_field('texto'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php elseif( get_row_layout() == 'modulo_1' ): ?>
            <div class="modulo modulo_1 mb-5 w-100 float-left row"> 
                <div class="col-11 mx-auto mx-md-0 col-md-6 mb-4 mb-md-0 py-5 text-left rounded-right" style="background: url('<?php the_sub_field('imagem'); ?>') center top no-repeat; background-size:cover; min-height: 40vh">
                    &nbsp;
                </div>                         
                <div class="col-12 col-md-6 col-xl-6 mb-md-0 py-3">
                    <p class="sm color mb-2 <?php the_sub_field('cor'); ?>"><?php the_sub_field('nome'); ?></p>
                    <p class="h3 p-0 mb-3 col-12 col-md-8"><?php the_sub_field('chamada'); ?></p>
                    <p><?php the_sub_field('texto'); ?></p>
                    <a class="mt-2 color <?php the_sub_field('cor'); ?>" href="#"><p class="mb-0"><?php the_sub_field('texto_do_link'); ?></p></a>
                    <blockquote class="rounded col-12 col-md-10 p-3 my-2 text-white bg <?php the_sub_field('cor'); ?>"><p class="mb-0"><?php the_sub_field('texto_de_citação'); ?></p></blockquote>
                    <p class="float-left py-2 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('profissional'); ?></p>
                </div>      
            </div>
        <?php elseif( get_row_layout() == 'modulo_2' ): ?>
            <div class="modulo col-12 col-md-10 offset-md-1"> 
                <div class="col-12 col-md-4 col-xl-6 mb-md-0 float-left">
                    <p class="sm color mb-2 <?php the_sub_field('cor'); ?>"><?php the_sub_field('nome'); ?></p>
                    <p class="h3 p-0 mb-3 col-12 col-md-8"><?php the_sub_field('chamada'); ?></p>
                    <p><?php the_sub_field('texto'); ?></p>
                    <a class="float-left mt-2 color <?php the_sub_field('cor'); ?>" href="<?php the_sub_field('link'); ?>"><p><?php the_sub_field('texto_do_link'); ?></p></a>
                </div>
                <div class="col-11 offset-1 col-md-6 offset-md-0 p-0 text-right float-left">
                    <img src="<?php the_sub_field('imagem'); ?>" />
                </div>                            
            </div>
        <?php elseif( get_row_layout() == 'modulo_3' ): ?>
            <?php if (get_sub_field('position') === 'esquerda' ) { ?>
            <div class="col-12 col-md-10 offset-1 modulo_3 float-left mb-5 d-flex align-items-stretch">
                <div class="col-4 float-left py-4">
                    <p class="m-0 h3 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('chamada'); ?>
                    <?php the_sub_field('texto'); ?>
                    <button class="btn bg <?php the_sub_field('cor'); ?> text-white w-100 mt-4"><?php the_sub_field('texto_do_link'); ?></button>                
                </div>
                <div class="col-8 float-left rounded" style="z-index:2; background:url('<?php the_sub_field('imagem'); ?>') center center no-repeat; background-size:cover;">
                </div>
            </div>
            <?php } else { ?>
            <div class="col-12 col-md-10 offset-1 modulo_3 float-left mb-5 d-flex align-items-stretch">
                <div class="col-8 float-left rounded" style="z-index:2; background:url('<?php the_sub_field('imagem'); ?>') center center no-repeat; background-size:cover;">
                </div>            
                <div class="col-4 float-left py-4">
                    <p class="m-0 h3 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('chamada'); ?>
                    <?php the_sub_field('texto'); ?>
                    <button class="btn bg <?php the_sub_field('cor'); ?> text-white w-100 mt-4"><?php the_sub_field('texto_do_link'); ?></button>                
                </div>
            </div>
            <?php } ?>                
            <?php elseif( get_row_layout() == 'imagem-full' ): ?> 
            <div class="mb-5 w-100 float-left" style="background: url('<?php the_sub_field('imagem'); ?>') center top no-repeat; background-size: cover; height: 90vh">
                <?php if (get_sub_field('citacao')) { ?>                                  
                    <blockquote class="rounded-left col-xl-6 offset-xl-6 p-3 my-5 text-white bg <?php the_sub_field('cor'); ?>">
                        <p class="my-0"><?php the_sub_field('citacao'); ?></p>
                        <p class="my-0"><small><?php the_sub_field('profissional'); ?></small></p>
                    </blockquote>
                <?php } ?>
            </div>
            <?php elseif( get_row_layout() == 'dots' ): ?> 
            <div class="col-12 col-md-10 offset-md-1 mb-5 withdots">
                <div class="w-50 float-right">
                    <?php echo get_template_part('components/sevenDots'); ?>    
                </div>
            </div>   
            <?php elseif( get_row_layout() == 'blockwithicons' ): ?> 
            <div class="col-12 col-md-10 offset-md-1 mb-5 row">
                <?php if(have_rows('bloco')): ?>
                <?php while(has_sub_field('bloco')): ?>                  
                    <div class="col-6 col-md-3 text-center">
                        <i class="<?php the_sub_field('icone'); ?> fa-3x color <?php the_sub_field('cor'); ?> "></i>
                        <br clear="all" />
                        <p class="h4 color mt-1 mb-2 <?php the_sub_field('cor'); ?>"><?php the_sub_field('valor'); ?> 
                        <p><?php the_sub_field('texto'); ?></p>  
                    </div>
                <?php endwhile; ?>
                <?php endif; ?>                
            </div>                        
            <?php elseif( get_row_layout() == 'imagemfullcomtexto' ): ?> 
            <div class="mb-5 py-5 col-12 float-left mb-5" style="background: url('<?php the_sub_field('imagem'); ?>') center top no-repeat; background-size: cover;">
                <div class="col-12 col-md-5 offset-md-1 rounded imagemfullcomtexto p-5 row">
                    <div class="w-50 mx-auto mb-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet.png" alt="SimplesVet" class="logo" />
                    </div>
                    <p class="my-0"><?php the_sub_field('texto'); ?></p>
                    <a class="mx-auto btn bg <?php the_sub_field('cor'); ?> text-white mt-4"><?php the_sub_field('texto_do_link'); ?></a>
                </div>
            </div>
            

        <?php else: ?>
            <?php the_content(''); ?>
        <?php endif; ?>
    <?php endwhile; else : ?>
<?php
endif;
?>
