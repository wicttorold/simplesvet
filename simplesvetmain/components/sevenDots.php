<div class="sevendots justify-content-end" data-aos="fade-right" data-aos-duration="10">
    <div class="bg red" data-aos="fade-right" data-aos-duration="500" data-aos-offset="300"></div>
    <div class="bg red" data-aos="fade-right" data-aos-duration="700" data-aos-offset="300"></div>
    <div class="bg yellow" data-aos="fade-right" data-aos-duration="900" data-aos-offset="300"></div>    
    <div class="bg turquoise" data-aos="fade-right" data-aos-duration="1100" data-aos-offset="300"></div>
    <div class="bg turquoise" data-aos="fade-right" data-aos-duration="1300" data-aos-offset="300"></div>
    <div class="bg blue" data-aos="fade-right" data-aos-duration="1500" data-aos-offset="300"></div>
    <div class="bg purple" data-aos="fade-right" data-aos-duration="1700" data-aos-offset="300"></div>
    <div class="bg purple" data-aos="fade-right" data-aos-duration="1900" data-aos-offset="300"></div>
</div>