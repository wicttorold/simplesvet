<div class="w-100 fixed-top">
    <div class="w-100" id="topbar">
        <div class="container">
            <div class="row py-1">
                <div class="w-100">
                    <a href="//simples.vet" class="float-left p-0 m-0 d-none d-md-block mr-3">
                        <p class="p-0 m-0 float-left">
                            <small>Visite o site da Simplesvet</small>
                        </p>
                    </a>
                    <a href="//simples.vet/experimente/" target="_blank" class="btn btn-experimente btn-primary btn-sm bg text-white p-0 m-0 px-2 mt-1 float-right" style="font-size:0.7rem;">Experimente Simplesvet</a>
                </div>
                <!-- <div class="col-5 col-md-4 float-right text-right text-white fa-social">
                    <a alt="Link para o Facebook da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Facebook'); ?>"><i class="fab fa-xs fa-facebook-f"></i></a>
                    <a alt="Link para o Instagram da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Instagram'); ?>"><i class="fab fa-xs fa-instagram ml-2"></i></a>
                    <a alt="Link para o Youtube da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Youtube'); ?>"><i class="fab fa-xs fa-youtube ml-2"></i></a>
                    <a alt="Link para o Linkedin da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Linkedin'); ?>"><i class="fab fa-xs fa-linkedin-in ml-2"></i></a>
                </div> -->
            </div>
        </div>  
    </div>
    <div class="progress-container">
        <div class="progress-bar" id="myBar"></div>
    </div>  
</div>