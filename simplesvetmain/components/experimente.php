<div class="w-100 float-left p-0 m-0">
    <div id="experimente" class="pt-5 pb-2">
        <div class="container pt-5">
            <div class="row">
                <div class="col-12 text-center pt-5 pb-2 px-4 mt-5">
                    <p>Simplesvet é um sistema veterinário online para clínicas e petshops que tem a missão de simplificar a gestão pet.</p>
                    <button class="btn btn-info bg turquoise px-5">Experimente Grátis</button>
                    <div class="w-100 float-left mt-3">
                        <a href="http://simples.vet" title="Conheça o sistema" class="text-white"><p><small>Conheça o sistema.</small></p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 