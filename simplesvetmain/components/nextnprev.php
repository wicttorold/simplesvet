<section id="nextNprev" class="row">
    <div class="w-100 row justify-content-center py-5 p-0 m-0">
        <?php echo get_template_part("components/fiveDots"); ?>
    </div>     
    <div class="text-center mb-5 col-12 font-weight-light display-1 color turquoise">
        Você pode gostar desses aqui:
    </div>
    <?php 
    $args_p = array('posts_per_page' => 1, 'orderby' => 'rand','post__not_in' => $do_not_duplicate );
    query_posts($args_p); ?>
    <!-- the loop -->
    <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
        <div class="col-12 col-md-4 offset-md-2 float-left p-0 mb-3 pr-lg-3 prevPost">
            <div class="postInfo w-100 rounded p-0" onclick="loadUrl('<?php the_permalink(''); ?>')" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>'); background-size: cover; cursor: pointer">
                <div class="mask rounded px-3 py-3 d-flex align-items-end prevPost text-right">
                    <div class="w-100">
                        <?php if(get_field('capa_h1')): ?>
                            <p class="m-0 w-100 float-left">
                                <a href="<?php the_permalink(''); ?>"><?php the_field('capa_h1'); ?></a>
                            </p>
                        <?php else:  ?>
                            <p class="m-0 w-100 float-left">
                                <a href="<?php the_permalink(''); ?>"><?php the_title(''); ?></a>
                            </p>
                        <?php endif;  ?>
                        <?php if(get_field('personagem_local')): ?>
                            <p class="m-0"><small><?php the_field('personagem_local'); ?></small></p>
                        <?php else:  ?>
                            <p class="m-0"><small>Inserir Personagem / Local</small></p>
                        <?php endif;  ?>
                    </div>                                                      
                </div>
            </div>
        </div>
    <?php endwhile; else : endif;  wp_reset_postdata();
    $args_n = array('posts_per_page' => 1, 'orderby' => 'rand','post__not_in' => $do_not_duplicate );
    query_posts($args_n); ?>
    <!-- the loop -->
    <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
        <div class="col-12 col-md-4 float-left p-0 mb-5 pl-lg-3 nextPost">
            <div class="postInfo w-100 rounded p-0" onclick="loadUrl('<?php the_permalink(''); ?>')" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>'); background-size: cover; cursor: pointer">
                <div class="mask rounded px-3 py-3 d-flex align-items-end nextPost">
                    <div class="w-100">
                        <?php if(get_field('capa_h1')): ?>
                            <p class="m-0 w-100 float-left">
                                <a href="<?php the_permalink(''); ?>"><?php the_field('capa_h1'); ?></a>
                            </p>
                        <?php else:  ?>
                            <p class="m-0 w-100 float-left">
                                <a href="<?php the_permalink(''); ?>"><?php the_title(''); ?></a>
                            </p>
                        <?php endif;  ?>
                        <?php if(get_field('personagem_local')): ?>
                            <p class="m-0"><small><?php the_field('personagem_local'); ?></small></p>
                        <?php else:  ?>
                            <p class="m-0"><small>Inserir Personagem / Local</small></p>
                        <?php endif;  ?>
                    </div>                                                          
                </div>
            </div>
        </div>
    <?php endwhile; else : endif; wp_reset_postdata();?>                        
</section> 