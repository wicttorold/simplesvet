<?php get_header(); ?>
<main class="position-relative"> 
    <?php echo get_template_part('components/topMenu'); ?>
    <article class="container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row">  
            <div class="w-100 float-left py-4"></div>
            <div class="col-12 col-md-12">
                <h1 class="h4 mt-3 mb-5"><?php the_title(''); ?></h1>
            </div> 
        </div>
        <div class="row">
            <?php echo get_template_part('components/acfPageBuilder'); ?>
        </div> 
        <?php endwhile; endif; wp_reset_postdata(); ?>          
    </article>
    <?php echo get_template_part('/components/quemsomos'); ?>
</main>
<?php get_footer(''); ?>

<script type="text/javascript">
    $(document).ready(function() {
        workwithusH = $('#workwithus').height();
        if ( documentW >= 991){
            $("#equipeImgWUS").css("height",workwithusH+80);
            $("#workwithus").css("margin-top",workwithusH*0.35);
        }
        else {
            $("#equipeImgWUS").css("height",workwithusH-100);
            $("#equipeImgWUS").removeClass("rounded-right");
            $("#workwithus").removeClass("rounded-left");
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        quemSomosH = $('#quemsomos').height();
        if ( documentW >= 991){
            $(".modulo blockquote").css("margin-left",-100);
            $("#equipeImg").css("height",quemSomosH+120);
            $("#quemsomos").css("margin-top",-quemSomosH*1.35);
        }
        else {
            $("#equipeImg").css("height",quemSomosH-100);
            $("#equipeImg").removeClass("rounded-right");
            $("#quemsomos").removeClass("rounded-left");
        }
        sliderSide = $('.sliderSide').height();
        $(".imgSide").css("height",sliderSide+170);
    });
    
</script>