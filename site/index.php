<?php get_header(); ?>

<main data-aos="fade-in" data-aos-duration="500">
    <?php echo get_template_part('components/topMenu'); ?>   
    <header class="container">
        <div class="row">
            <div class="col-10 offset-2 py-0 d-none d-md-block negative" style="margin-bottom: -8px; z-index: 100;">     
                <div class="col-5 offset-7 mb-2 p-0">   
                    <?php echo get_template_part('components/sevenDots'); ?>
                </div>
            </div>   
        </div>           
        <div class="row">
            <div class="col-8 col-sm-6 col-md-6 float-left py-4">
                <h2 class="h4 m-0 p-0"><?php echo single_term_title( "", false );?></h2>
            </div>     
        </div>
    </header>

    <div class="container">
        <div class="row" data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>
                <?php echo get_template_part("components/loop"); ?>
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>  


            <div class="w-100 row justify-content-center py-5 p-0 m-0">
                <?php echo get_template_part("components/fiveDots"); ?>
            </div>
        </main>

</main>

<?php get_footer(''); ?>