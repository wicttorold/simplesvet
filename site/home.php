<?php get_header(); ?>
<?php echo get_template_part('components/topMenu'); ?> 
<main data-aos="fade-in" data-aos-duration="500">   
    <div class="container-fluid">        
        <div class="row">
            <div id="carouselExampleIndicators" class="carousel slide w-100" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <?php if(get_field('slider', 'options')): ?>
                <div class="carousel-inner">
                <?php
                $sliderNum = 0;
                 while(has_sub_field('slider', 'options')): ?>

                    <div class="carousel-item float-left">
                        <style>
                            .carousel-item .imgSide.slide-<?php echo $sliderNum; ?> { 
                                background-image: -webkit-linear-gradient(0deg, transparent 66%, #fff), url(<?php the_sub_field('imagem'); ?>);
                                background-image: linear-gradient(90deg, transparent 66%, #fff), url(<?php the_sub_field('imagem'); ?>);
                            }

                            @media (max-width: 767px) {
                            .carousel-item .imgSide.slide-<?php echo $sliderNum; ?> {
                            background-image: url(<?php the_sub_field('imagem'); ?>);
                            background-position: 50% 50%;
                            background-size: cover;
                            background-repeat: no-repeat;
                            }
                            .carousel-item .h3 { font-size: 1.5rem; line-height: 1.6rem; min-height: 100px; }
                            }
                        </style>
                        <div class="col-12 col-md-8 p-0 float-left imgSide slide-<?php echo $sliderNum; ?>">
                        </div>
                        <div class="col-12 col-md-4 pt-2 mt-2 float-left sliderSide">
                            <p class="h3"><?php the_sub_field('titulo'); ?></p>
                            <p class=""><?php the_sub_field('paragrafo'); ?></p>
                            <button class="btn text-white rounded-extra bg <?php the_sub_field('cor'); ?>"><?php the_sub_field('texto_do_botao'); ?></button>
                        </div>
                    </div>

                <?php $sliderNum++; endwhile; ?>
                </div>
                <?php endif; ?>
                
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-10 offset-2 py-0 d-none d-md-block negative" style="margin-top: -4px; z-index: 100;">     
                <div class="col-7 offset-5 mb-2 p-0">   
                    <?php echo get_template_part('components/sevenDots'); ?>
                </div>
            </div>   
        </div>
    </div>    
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-xl-10 offset-xl-1 text-center float-left pt-5 pb-3">
                <p class="h4">Sistema veterinário online para clínicas veterinárias e pet shops</p>
                <p class="h5">Conheça alguns dos recursos</p>
            </div>   
        </div>
    </div>  
    <?php if(get_field('editor_visual', 'options')): ?>
    <?php while(has_sub_field('editor_visual', 'options')): ?>
        <?php if( get_row_layout() == 'modulo' ): ?>    
            <div class="container py-2 py-md-4 modulo">
                <div class="row">
                    <div class="paragraph_image container-fluid float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                        <div class="row"> 
                            <div class="col-11 mx-auto mx-md-0 col-md-6 mb-4 mb-md-0 py-5 text-left rounded-right" style="background: url('<?php the_sub_field('imagem'); ?>') center top no-repeat; background-size:cover; min-height: 40vh">
                                &nbsp;
                            </div>                         
                            <div class="col-12 col-md-6 col-xl-6 mb-md-0 py-3">
                                <p class="sm color mb-2 <?php the_sub_field('cor'); ?>"><?php the_sub_field('nome'); ?></p>
                                <p class="h3 p-0 mb-3 col-12 col-md-8"><?php the_sub_field('chamada'); ?></p>
                                <p><?php the_sub_field('texto'); ?></p>
                                <p><?php the_sub_field('texto_do_link'); ?></p>
                                <blockquote class="rounded col-12 col-md-10 p-3 my-2 text-white bg <?php the_sub_field('cor'); ?>"><p><?php the_sub_field('citacao'); ?></p></blockquote>
                                <p class="float-left py-2 color <?php the_sub_field('cor'); ?>"><?php the_sub_field('profissional'); ?></p>
                            </div>      
                        </div>
                    </div>            
                </div>
            </div>
        <?php elseif( get_row_layout() == 'modulo2' ): ?>    
            <div class="container bg-none py-2 py-md-4 modulo" >
                <div class="row">
                    <div class="paragraph_image container-fluid float-left" data-aos="zoom-in" data-aos-duration="700" data-aos-offset="50">
                        <div class="row"> 
                            <div class="col-12 col-md-4 col-xl-6 mb-md-0">
                                <p class="sm color mb-2 <?php the_sub_field('cor'); ?>"><?php the_sub_field('nome'); ?></p>
                                <p class="h3 p-0 mb-3 col-12 col-md-8"><?php the_sub_field('chamada'); ?></p>
                                <p><?php the_sub_field('texto'); ?></p>
                                <a class="float-left mt-2 color <?php the_sub_field('cor'); ?>" href="<?php the_sub_field('link'); ?>"><p><?php the_sub_field('texto_do_link'); ?></p></a>
                            </div>
                            <div class="col-11 offset-1 col-md-6 offset-md-0 p-0 text-right">
                                <img src="<?php the_sub_field('imagem'); ?>" />
                            </div>                            
                        </div>
                    </div>            
                </div>
            </div>
        <?php elseif( get_row_layout() == 'imagem-full' ): ?> 
        <div class="container-fluid bgParallax my-5 my-md-3 py-2 py-md-4" data-speed="20" style="background: url('<?php the_sub_field('imagem'); ?>') center center no-repeat; background-size: cover; height: 90vh">
            <div class="row">                     
                <blockquote class="rounded-left col-xl-6 offset-xl-6 p-3 my-2 text-white bg <?php the_sub_field('cor'); ?>">
                    <p class="my-0"><?php the_sub_field('citacao'); ?></p>
                    <p class="my-0"><small><?php the_sub_field('profissional'); ?></small></p>
                </blockquote>
            </div>
        </div>
        <?php elseif( get_row_layout() == 'precos' ): ?> 
        <div class="container-fluid ">
            <div class="row">
                <div class="w-100 mt-2 mb-2 mt-md-3 mb-md-5 py-2 py-md-4" style="background: url('<?php the_sub_field('imagem'); ?>') top center no-repeat; background-size: cover;">
                    <div class="col-12 offset-0 col-md-10 offset-md-1 text-white pb-5">
                        <p class="h4 mb-4 mb-md-2 pb-2 pb-md-2 text-center text-md-left mt-5 mt-md-2"><?php the_sub_field('chamada'); ?></p>
                        <form class="col-12 col-md-6 px-0 float-left">
                            <div class="col-12 col-md-6 px-0 mb-2 float-left">
                                <input type="email" class="form-control float-left rounded-extra w-100" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Informe seu e-mail">
                            </div>
                            <div class="col-12 col-md-6 px-0 px-md-2 mb-2 float-left">
                                <button type="submit" class="btn btn-primary bg purple rounded-extra float-left w-100">Teste grátis por 10 dias</button>
                                <a href="#"><p class="text-white text-center w-100"><small>ou confira todos os planos</small></p></a>
                            </div>
                        </form>
                    </div>
                    <div class="col-10 offset-1 text-white py-2 pt-md-5 mb-0 row">
                        <div class="col-12 col-md-6 px-0">
                            <p><?php the_sub_field('chamada_inferior'); ?></p>
                            <p><?php the_sub_field('terceira_chamada'); ?></p>
                        </div>
                        <?php if(have_rows('ícones')): ?>
                            <?php while(has_sub_field('ícones')): ?> 
                                <div class="col-12 col-md-2 px-0 text-center icones mt-3">
                                    <div style="height:61px;"><img src="<?php the_sub_field('icone'); ?>" /></div>
                                    <p class="dado py-3 my-0"><?php the_sub_field('dado'); ?></p>
                                    <p class="py-0 my-0"><?php the_sub_field('descricao'); ?></p>
                                </div>   
                            <?php endwhile; ?>
                        <?php endif; ?>                                                                           
                    </div>
                </div>               
            </div>
        </div>        
             
            
        <?php endif; ?>
    <?php endwhile; ?>
    <?php endif; ?>    


    <?php echo get_template_part('/components/quemsomos'); ?>

</main>

<?php get_footer(''); ?>

<script type="text/javascript">
    $(document).ready(function() {
        quemSomosH = $('#quemsomos').height();
        if ( documentW >= 991){
            $(".modulo blockquote").css("margin-left",-100);
            $("#equipeImg").css("height",quemSomosH+120);
            $("#quemsomos").css("margin-top",-quemSomosH*1.35);
        }
        else {
            $("#equipeImg").css("height",quemSomosH-100);
            $("#equipeImg").removeClass("rounded-right");
            $("#quemsomos").removeClass("rounded-left");
        }
        sliderSide = $('.sliderSide').height();
        $(".imgSide").css("height",sliderSide+170);
    });
    
</script>