<nav class="p-0 px-1 p-3 px-md-5">
    <i class="fas fa-times fa-2x menucall my-0 mt-1 my-md-3 float-right"></i>
    <div class="w-100 float-right"></div>
    <p class="h4 my-2 float-left w-100 p-0 font-weight-light">Outras Histórias</p>
    <ul class="text-left float-left w-100 my-2 p-0 list-unstyled">
        <?php 
        // the query to set the posts per page to 3
        wp_reset_postdata(); wp_reset_query();
        $args = array('posts_per_page' => 5 );
        query_posts($args); ?>
        <!-- the loop -->
        <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
            <li class="mb-3 mb-md-2 ml-3 color-" style="list-style-type: disc;"><a href="<?php the_permalink(''); ?>" alt="Leia '<?php the_title(''); ?>'"><?php the_title(''); ?></a></li>
        <?php endwhile; ?>
        <?php else : ?>
        <!-- No posts found -->
        <?php endif; wp_reset_postdata(); wp_reset_query();?>  

        <a href="<?php bloginfo('url'); ?>/historias" class="btn btn-outline float-right my-1 my-md-2 btn-sm blue">Ver todas</a>
    </ul>
    <div class="w-100 float-left">
        <?php wp_nav_menu( array( 'theme_location' => 'navMenu_side', 'menu_class' => 'list-unstyled' ) ); ?>
    </div>
    <div class="w-100 text-center px-0 float-left mt-1 mb-2">
        <a href="<?php bloginfo( 'url' ); ?>" alt="Ir para a capa"  class="col-7 col-md-7 col-xl-5 mx-auto mb-1 d-block" >
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet-gestao.png" alt="SimplesVet - Gestão pet suu" class="logo" />
        </a>    
    </div>   
</nav>
