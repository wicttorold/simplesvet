<div class="w-100 fixed-top">
    <div class="w-100" id="topbar">
        <div class="container">
            <div class="row py-1">
                <div class="col-7 col-md-8">
                    <a href="//simples.vet" class="float-left p-0 m-0 d-md-block mr-3">
                        <p class="p-0 m-0 float-left">
                            <small>Visite o site da Simplesvet</small>
                        </p>
                    </a>
                </div>
                <div class="col-5 col-md-4 float-right text-right text-white fa-social pl-0">
                    <a alt="Link para o Facebook da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Facebook'); ?>"><i class="fab fa-xs fa-facebook-f"></i></a>
                    <a alt="Link para o Instagram da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Instagram'); ?>"><i class="fab fa-xs fa-instagram ml-1"></i></a>
                    <a alt="Link para o Youtube da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Youtube'); ?>"><i class="fab fa-xs fa-youtube ml-1"></i></a>
                    <a alt="Link para o Linkedin da Simplesvet" target="_blank" href="<?php echo get_theme_mod('svRedes_Linkedin'); ?>"><i class="fab fa-xs fa-linkedin-in ml-1"></i></a>
                </div>
            </div>
        </div> 
    </div>
    <div class="progress-container">
        <div class="progress-bar" id="myBar"></div>
    </div>
</div>