<?php echo get_template_part( 'components/fixedMenu'); ?>

<div class="position-absolute w-100" style="z-index:90;">
    <header class="container my-3 pt-5 pt-md-3">
        <div class="row">
        <div class="col-12 py-2 px-md-3 pt-md-4 pb-md-0 d-none d-md-block">     
            <div class="col-6 col-md-5 float-right mb-2 p-0">   
                <?php echo get_template_part('components/sevenDots'); ?>
            </div>
        </div>
        <div class="col-8 col-sm-6 col-md-6 float-left">
            <div class="row align-items-end">
                <a href="<?php bloginfo( 'url' ); ?>" alt="Ir para a capa"  class="col-10 col-lg-6 float-left" >
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet-alt.png" alt="SimplesVet" class="logo" />
                </a>
                <p class="col-10 col-lg-6 float-left text-right text-lg-left text-white mb-2 sitename"><?php bloginfo('description');?></p>
            </div>
        </div>
        <div class="col-2 offset-2 offset-sm-4 offset-md-0 col-md-6 float-right text-right text-white pt-0 pt-md-2 pr-md-3" id="menu">
            <small><i class="fas fa-bars fa-2x menucall pt-1"></i></small>
        </div>         
        </div>
    </header>
</div>