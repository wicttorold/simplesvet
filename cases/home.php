<?php get_header(); ?>
<?php echo get_template_part('components/topMenuB'); ?> 
<main data-aos="fade-in" data-aos-duration="500">
    <div class="container">       
        <div class="row">
            <div class="col-8 col-sm-6 col-md-6 float-left py-4">
                <h2 class="h4 m-0 p-0"><?php echo single_term_title( "", false );?></h2>
            </div>     
        </div>
    </div>
    <div class="container-fluid home p-0 m-0">
        <div class="loop row p-0 m-0">
            <div class="col-12 p-0 m-0">
                <style>
                    #posts .post {
                    width: 50%;
                    margin: 0;
                    float: left;
                    height: 20vh;
                    }
                </style>
                <div class="row p-0 m-0" id="posts">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>

                    <div onclick="loadUrl('<?php the_permalink(''); ?>')" class="post row mb-4" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>') center center no-repeat; background-size: cover; cursor: pointer"  data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
                        <div class="m-0 p-0 w-100">
                        <div class="mask p-1 p-md-5 w-100 align-items-center row m-0 pb-2 pb-md-5">
                            <div class="postInfo col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-8 offset-xl-2">                            
                                <p class="m-0">
                                <?php if(get_field('capa_h1')): ?>
                                <a href="<?php the_permalink(''); ?>"><?php the_field('capa_h1'); ?></a></p>
                                <?php else:  ?>
                                <a href="<?php the_permalink(''); ?>"><?php the_title(''); ?></a></p>
                                <?php endif;  ?>
                                <?php if(get_field('personagem_local')): ?>
                                <p class="m-0"><small><?php the_field('personagem_local'); ?></small></p>
                                <?php else:  ?>
                                <p class="m-0"><small>Inserir Personagem / Local</small></p>
                                <?php endif;  ?>                                
                            </div>
                        </div>
                        </div>
                    </div>

                    <?php endwhile; ?>
                        <div class="w-100" data-aos="fade-in" data-aos-duration="2000" data-aos-offset="300">
                            <?php
                            if ( function_exists('bootstrap_pagination') )
                            bootstrap_pagination();
                            ?>
                        </div>
                    <?php else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>  
                    <div class="w-100 row justify-content-center py-5 p-0 m-0">
                        <?php echo get_template_part("components/fiveDots"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(''); ?>