<?php get_header(); ?>
<main class="position-relative">
    <?php echo get_template_part('components/topMenu'); ?>
    <article class="container-fluid">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="post mb-5 row">  
            <div class="mask w-100 m-0 pb-3 row align-items-end">
                <div class="postInfo text-center col py-3 p-0 text-white">
                    <h1 class="m-0"><?php the_field('personagem_local'); ?></h1>
                    <h2><?php the_title(''); ?></h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 p-0" data-aos="fade-in" data-aos-duration="1000" data-aos-offset="200">
                    <?php the_content(''); ?>
                </div>
            </div>                      
        </div>         
        <?php endwhile; endif; wp_reset_postdata(); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 float-left pl-3 mb-3">
                <div class="row postsLoop">
                    <?php 
                    // the query to set the posts per page to 3
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array('posts_per_page' => 6, 'paged' => $paged );
                    query_posts($args); ?>
                    <!-- the loop -->
                    <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                    <div class="col-12 col-md-6 d-flex align-items-stretch mb-4" data-aos="fade-in" data-aos-duration="2000" data-aos-offset="100">
                        <div class="postInfo col-12 rounded p-0" onclick="loadUrl('<?php the_permalink(''); ?>')" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>'); background-size: cover; cursor: pointer">
                            <div class="mask rounded px-3 py-3 d-flex align-items-end">
                                <?php if(get_field('capa_h1')): ?>
                                <p class="m-0 text-white w-100">
                                    <a href="<?php the_permalink(''); ?>"><?php the_field('capa_h1'); ?></a>
                                </p>
                                <?php else:  ?>
                                <p class="m-0 text-white w-100">
                                    <a href="<?php the_permalink(''); ?>"><?php the_title(''); ?></a>
                                </p>
                                <?php endif;  ?>                                                  
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <div class="w-100" data-aos="fade-in" data-aos-duration="2000" data-aos-offset="300">
                        <?php
                        if ( function_exists('bootstrap_pagination') )
                        bootstrap_pagination();
                        ?>
                    </div>
                    <?php else : ?>
                    <!-- No posts found -->
                    <?php endif; ?>
                </div>           
                </div>
            </div>                      
        </div>          
        <div class="row justify-content-center py-5">
            <?php echo get_template_part("components/fiveDots"); ?>
        </div>            
    </article>   
</main>
<?php get_footer(''); ?>