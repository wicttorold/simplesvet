<?php get_header(); ?>
<main class="position-relative">
    <?php echo get_template_part('components/topMenu'); ?>
    <div class="container-fluid">
        <div class="loop row">
            <div class="col-12">
                <?php 
                wp_reset_query();
                $args = array('posts_per_page' => 4, 'post__not_in' => $do_not_duplicate );
                query_posts($args);
                if (have_posts()) : ?>
                <section id="posts">
                    <?php while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
                        <div onclick="loadUrl('<?php the_permalink(''); ?>')" class="post row mb-4" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>') center center no-repeat; background-size: cover; cursor: pointer"  data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
                            <div class="m-0 p-0 w-100">
                            <div class="mask p-1 p-md-5 w-100 align-items-center row m-0 pb-2 pb-md-5">
                                <div class="postInfo col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-xl-8 offset-xl-2">                            
                                    <p class="m-0">
                                    <?php if(get_field('capa_h1')): ?>
                                    <a href="<?php the_permalink(''); ?>"><?php the_field('capa_h1'); ?></a></p>
                                    <?php else:  ?>
                                    <a href="<?php the_permalink(''); ?>"><?php the_title(''); ?></a></p>
                                    <?php endif;  ?>
                                    <?php if(get_field('personagem_local')): ?>
                                    <p class="m-0"><small><?php the_field('personagem_local'); ?></small></p>
                                    <?php else:  ?>
                                    <p class="m-0"><small>Inserir Personagem / Local</small></p>
                                    <?php endif;  ?>                                
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </section>
                <div class="load_more text-center mb-4">
                    <a class="btn bg blue text-white" onClick="onClick()" data-link="/historias/page/2">Ver mais posts</a>
                </div>
                <script type="text/javascript">
                    jQuery('.load_more a').on('click', function(e){
                        var link = jQuery(this).attr('data-link');
                        $.get(link, function(data) {
                        var post = $("#posts .post ", data);
                            $('#posts').append(post);                            
                        });
                    });
                    var urlNum = 2;                
                    function onClick() {
                        var urlNew = '/historias/page/' + urlNum;                                              
                        $('.load_more a').attr("data-link",urlNew);
                        urlNum += 1;
                        $.get(urlNew, function( data ) {
                        }).fail(function(){ 
                            $('.load_more').html('<div class="btn bg blue text-white" style="cursor:default">Ops! Por hoje é só...</div>');
                        });

                    };                    
                </script>
                <?php endif;  ?>
            </div>
        </div>    
    </div>
</main>
<?php get_footer(''); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $( ".postInfo:first" ).removeClass("col-12 col-md-8 offset-md-2 col-xl-8 offset-xl-2");
    });
</script>