<?php get_header(); ?>
<main class="position-relative"> 
    <?php echo get_template_part('components/topMenu'); ?>
    <div class="container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row">  
            <div class="col-12 col-md-12 withdots">
                <div class="w-50 float-left">
                    <h1 class="h5"><?php the_title(''); ?></h1>
                </div>
                <div class="w-50 float-left pt-2">
                    <div class="col-12 float-right"><?php echo get_template_part('components/sevenDots'); ?></div>
                </div>
            </div> 
        </div>
    </div>
    <div class="container-fluid mb-4">
        <div class="row">
            <div class="col-12 p-0">
                <img class="w-100" src="<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>" />
            </div>
        </div>
    </div>
    <article class="container-fluid">
        <div class="row">
            <?php echo get_template_part('components/acfPageBuilder'); ?>
        </div> 
        <?php endwhile; endif; wp_reset_postdata(); ?>          
    </article>
    <?php echo get_template_part('/components/quemsomos'); ?>
</main>
<?php get_footer(''); ?>

<script type="text/javascript">
    $(document).ready(function() {
        workwithusH = $('#workwithus').height();
        if ( documentW >= 991){
            $("#equipeImgWUS").css("height",workwithusH+80);
            $("#workwithus").css("margin-top",workwithusH*0.35);
        }
        else {
            $("#equipeImgWUS").css("height",workwithusH-100);
            $("#equipeImgWUS").removeClass("rounded-right");
            $("#workwithus").removeClass("rounded-left");
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        quemSomosH = $('#quemsomos').height();
        if ( documentW >= 991){
            $(".modulo blockquote").css("margin-left",-100);
            $("#equipeImg").css("height",quemSomosH+120);
            $("#quemsomos").css("margin-top",-quemSomosH*1.35);
        }
        else {
            $("#equipeImg").css("height",quemSomosH-100);
            $("#equipeImg").removeClass("rounded-right");
            $("#quemsomos").removeClass("rounded-left");
        }
        sliderSide = $('.sliderSide').height();
        $(".imgSide").css("height",sliderSide+170);
    });
    
</script>