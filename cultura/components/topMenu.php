<?php echo get_template_part( 'components/fixedMenu'); ?>
<div id="topmenu" class="w-100" style="position: relative;">
    <header class="container">
        <div class="row pt-5 pb-3">
            <div class="col-12 col-sm-6 col-md-5 float-left row pr-0">
                <div class="col-8 pr-0">
                    <a href="<?php bloginfo( 'url' ); ?>" alt="Ir para a capa" >
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet.png" alt="SimplesVet" class="logo" />
                    </a>
                </div>
                <button class="col-4 navbar-toggler float-right pr-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars float-right color gray d-block d-md-none p-0"></i>
                </button> 
            </div>
            <div class="col-12 col-md-7 float-right text-right pr-0">
                <div class="navbar navbar-expand-lg pr-0">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link pr-0 pl-4" href="/recursos">Recursos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pr-0 pl-4" href="/precos">Preços</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link pr-0 pl-4" href="/quem-somos">Quem Somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link pr-0 pl-4" href="#">Já sou cliente</a>
                    </li>      
                    </ul>
                </div>
                </div>
            </div>         
        </div>
    </header>
</div>

