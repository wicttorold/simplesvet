<?php get_header(); ?>
<?php echo get_template_part('components/topMenu'); ?>  
<main data-aos="fade-in" data-aos-duration="500">
 
    <div class="container">
        <div class="row">
            <div class="col-10 offset-2 py-0 d-none d-md-block negative" style="margin-bottom: -8px; z-index: 100;">     
                <div class="col-5 offset-7 mb-2 p-0">   
                    <?php echo get_template_part('components/sevenDots'); ?>
                </div>
            </div>   
        </div>           
        <div class="row">
            <div class="col-8 col-sm-6 col-md-6 float-left py-4">
                <h2 class="h4 m-0 p-0">Novos Artigos</h2>
            </div>     
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <?php 
            // the query to set the posts per page to 3
            $args = array('posts_per_page' => 1 );
            query_posts($args); ?>
            <!-- the loop -->
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>

                <div class="d-block d-md-none float-left">
                    <?php echo get_template_part("components/loop"); ?>
                </div>
                    <div class="d-none d-md-block col-12 col-sm-12 col-md-9 float-left position-relative featOne" data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
                        <div class="col-12 p-0 float-left">
                            <a href="<?php the_permalink(''); ?>">
                                <img src="<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>" class="rounded w-100" />
                            </a>
                            <div class="mask w-100 rounded-bottom position-absolute p-3" style="bottom:0px;">
                                <div <?php post_class('w-100 categoryLink'); ?>>
                                    <?php the_category( ' ,'); ?>
                                </div>
                                <div class="w-100 float-left">
                                </div>
                                <a href="<?php the_permalink(''); ?>" class="w-75 float-left postTitle">
                                    <?php the_title( ''); ?>
                                </a>
                            </div>
                        </div>
                    </div>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; wp_reset_query(); ?>
            
            <div class="col-12 col-sm-12 col-md-3 float-left featTwo mt-4 mt-md-0 " data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
                <?php 
                // the query to set the posts per page to 3
                $args = array('posts_per_page' => 2, 'post__not_in' => $do_not_duplicate );
                
                query_posts($args); ?>
                <!-- the loop -->    
                <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
                    <div class="col-12 p-0 float-left">
                        <a href="<?php the_permalink(''); ?>">
                            <img src="<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>" class="rounded w-100" />
                        </a>
                        <div class="w-100 rounded-bottom px-0 py-2">
                            <div <?php post_class('w-100 categoryLink mb-1'); ?>>
                                <?php the_category( ' ,'); ?>
                            </div>
                            <div class="w-100 float-left">
                            </div>
                            <a href="<?php the_permalink(''); ?>" class="w-100 float-left postTitle mb-3">
                                <?php the_title( ''); ?>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php else : ?>
                <?php endif; ?>  
            </div>

            <div class="col-12 float-left text-center my-4">
                <button class="btn btn-light bg blue text-white btn-sm py-2 px-4">Ver mais posts</button>
            </div>

            <div class="col-12 rounded">
                <div class="col-12 rounded pt-3 float-left" id="formNews">
                    <p class="col-12 col-md-5 float-left">
                        Receba dicas GRATUITAS sobre gestão para pet shops e clínicas veterinárias!
                    </p>
                    <div class="mx-0 px-0 col-12 col-md-7 row pt-1">
                        <div class="col-12 col-md-9 mb-3 mb-md-0">
                            <input type="email" class="form-control float-left" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Informe seu e-mail" />
                        </div>
                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                            <button type="submit" class="btn btn-primary bg blue text-white float-right w-100 btn-sm">
                                Enviar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row d-flex align-items-stretch">
            <div class="col-12 mt-5 mb-4 float-left">
                <h2 class="h4 m-0 p-0">
                    Artigos por área
                </h2>
            </div>    
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg blue text-white rounded py-3 text-center btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/gestao')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/cachorro2.png" alt='Gestão' /><br />
                        Gestão
                    </div>
                </div> 
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg purple rounded py-3 text-center text-white btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/marketing')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/gato.png" alt='Marketing' /><br />
                        Marketing
                    </div>
                </div>
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg red rounded py-3 text-center text-white btn-cat" style="height:100%" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/vendas')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/cachorro1.png" alt='Vendas' /><br />                
                        Vendas
                    </div>
                </div> 
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg yellow rounded py-3 text-center text-white btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/sucesso-do-cliente')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/passaro.png" alt='Sucesso do Cliente' /><br />
                        Sucesso do Cliente
                    </div>
                </div>           
        </div>

        <div id="populares" class="row" data-aos="fade-out" data-aos-duration="700"  data-aos-offset="120">
            <div class="col-12 mt-5 mb-4 float-left">
                <h2 class="h4 m-0 p-0">
                    Artigos mais populares
                </h2>
            </div>             
            <?php $args = array('posts_per_page' => 1, 'category__and' => 9, 'post__not_in' => $do_not_duplicate );
            query_posts($args); ?>
            <!-- the loop -->    
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
                <?php echo get_template_part("components/loop"); ?>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>  
            <?php $args = array('posts_per_page' => 1, 'category__and' => 7, 'post__not_in' => $do_not_duplicate );
            query_posts($args); ?>
            <!-- the loop -->    
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID   ?>
                <?php echo get_template_part("components/loop"); ?>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>  
            <?php $args = array('posts_per_page' => 1, 'category__and' => 6, 'post__not_in' => $do_not_duplicate );
            query_posts($args); ?>
            <!-- the loop -->    
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
                <?php echo get_template_part("components/loop"); ?>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>  
            <?php $args = array('posts_per_page' => 1, 'category__and' => 8, 'post__not_in' => $do_not_duplicate );
            query_posts($args); ?>
            <!-- the loop -->    
            <?php if ( have_posts() ) : while (have_posts()) : the_post(); $do_not_duplicate[] = $post->ID ?>
                <?php echo get_template_part("components/loop"); ?>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>                                      
        </div>
        
        <div class="w-100 row justify-content-center py-5 p-0 m-0">
            <?php echo get_template_part("components/fiveDots"); ?>
        </div>     
    </div>
    
    <?php echo get_template_part('/components/quemsomos'); ?>


</main>

<?php get_footer(''); ?>

<script type="text/javascript">
    $(document).ready(function() {
        quemSomosH = $('#quemsomos').height();
        if ( documentW >= 991){
            $("#quemsomos").css("margin-top",-quemSomosH*1.35);
            $("#equipeImg").css("height",quemSomosH+120);
        }
        else {
            $("#equipeImg").removeClass("rounded-right");
            $("#quemsomos").removeClass("rounded-left");
            $("#equipeImg").css("height",quemSomosH);
        }
    });
</script>