<?php get_header(); ?>
<main class="position-relative">
    <?php echo get_template_part('components/topMenu'); ?>   
    <section id="single" class="container-fluid">       
        <article id="articlebox" class="row">
            <div class="col-11 float-right py-0 pr-1 d-none d-md-block negative" style="margin-bottom: -8px; z-index: 80;">     
                <div class="col-5 offset-7 mb-2 p-0">   
                    <?php echo get_template_part('components/sevenDots'); ?>
                </div>
            </div>             
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>
            <div id="post" class="w-100 position-relative" style="background: url('<?php echo get_thumb_full( get_post_thumbnail_id( $post->ID )); ?>') no-repeat center center; background-size: cover">  
                <div class="mask position-relative align-content-end text-left row m-0">
                    <div class="d-block col-10 col-md-8 col-xl-6 position-relative mx-auto">
                            <div <?php post_class('w-100 categoryLink negative'); ?>>
                                <?php the_category( ' ,'); ?>
                            </div>                        
                        <h1 class="h2 w-100 title py-3 my-0"><?php the_title(''); ?></h1>
                    </div>
                </div>
            </div>
            <div class="pt-4 w-100">            
                <?php get_sidebar(''); ?>
                <?php echo get_template_part('components/acfEditor'); ?>
            </div>
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria'); ?></p>
            <?php endif; ?>           
        </article>
        <?php echo get_template_part('components/nextnprev'); ?>
    </section>
</main>
<?php get_footer(''); ?>

<script>
// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};
function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/readingTime.min.js"></script>
<script type="text/javascript">
$(function() {
    $('article').readingTime({
    wordCountTarget: 'eta',
    lang: 'pt',
    wordsPerMinute: '190',
    });

});
</script>
<script type="text/javascript">
$(document).ready(function() {
if ( documentW >= 991){

    $('div.bgParallax').each(function(){
    var $obj = $(this);
    $(window).scroll(function() {
        var yPos = -($(window).scrollTop() / $obj.data('speed') +200) ; 
        console.log(yPos);
        var bgpos = 'center '+ yPos + 'px';
        $obj.css('background-position', bgpos );
    });
    
});

}
else {
}
});
</script>