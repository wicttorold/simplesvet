<?php get_header(); ?>
<?php echo get_template_part('components/topMenu'); ?> 
<main data-aos="fade-in" data-aos-duration="500">
    <header class="container">
        <div class="row">
            <div class="col-10 offset-2 py-0 d-none d-md-block negative" style="margin-bottom: -8px; z-index: 100;">     
                <div class="col-5 offset-7 mb-2 p-0">   
                    <?php echo get_template_part('components/sevenDots'); ?>
                </div>
            </div>   
        </div>           
        <div class="row">
            <div class="col-8 col-sm-6 col-md-6 float-left py-4">
                <h2 class="h4 m-0 p-0"><?php echo single_term_title( "", false );?></h2>
            </div>     
        </div>
    </header>

    <div class="container">
        <div class="row" id="postLoop">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID ?>
                <?php echo get_template_part("components/loop"); ?>
                <?php endwhile; ?>

                <div class="load_more text-center mb-4 mx-auto">
                    <a class="btn bg blue text-white" onClick="onClick()" data-link="/historias/page/2">Ver mais posts</a>
                </div>
                <script type="text/javascript">
                jQuery('.load_more a').on('click', function(e){
                    var link = jQuery(this).attr('data-link');
                    $.get(link, function(data) {
                        var post = $("#postLoop", data);
                        $('#moreposts').append(post);                            
                    });
                });                
                function onClick() {
                    var urlNum = 2;
                    var urlNew = '/categoria/marketing/page/' + urlNum;                                              
                    $('.load_more a').attr("data-link",urlNew);
                    $.get(urlNew, function( data ) {
                    }).fail(function(){ 
                        $('.load_more').html('<div class="btn bg blue text-white" style="cursor:default">Ops! Por hoje é só...</div>');
                    });
                    urlNum += 1;
                    console.log(urlNum);
                };                    
                </script>                

                <?php else: ?>
            <?php endif; ?>
            <div id="moreposts"></div>
        </div>              
            <div class="w-100 row justify-content-center py-5 p-0 m-0">
                <?php echo get_template_part("components/fiveDots"); ?>
            </div>        
    </div>
    <div class="container">
        <div class="row d-flex align-items-stretch">
            <div class="col-12 mt-5 mb-4 float-left">
                <h2 class="h4 m-0 p-0">
                    Artigos por área
                </h2>
            </div>    
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg blue text-white rounded py-3 text-center btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/gestao')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/cachorro2.png" alt='Gestão' /><br />
                        Gestão
                    </div>
                </div> 
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg purple rounded py-3 text-center text-white btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/marketing')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/gato.png" alt='Marketing' /><br />
                        Marketing
                    </div>
                </div>
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg red rounded py-3 text-center text-white btn-cat" style="height:100%" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/vendas')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/cachorro1.png" alt='Vendas' /><br />                
                        Vendas
                    </div>
                </div> 
                <div class="col-6 mb-4 mb-md-0 col-md-3">
                    <div class="col-12 bg yellow rounded py-3 text-center text-white btn-cat" onclick="loadUrl('<?php bloginfo('url'); ?>/categoria/sucesso-do-cliente')">
                        <img class="mb-2" src="<?php echo get_template_directory_uri(); ?>/img/passaro.png" alt='Sucesso do Cliente' /><br />
                        Sucesso do Cliente
                    </div>
                </div>           
        </div>
    </div>
</main>

<?php get_footer(''); ?>

