<nav class="p-0 px-1 p-3 px-md-5">
    <i class="fas fa-times fa-2x menucall my-0 mt-1 my-md-3 float-right"></i>
    <div class="w-100 float-right"></div>
    <p class="h4 my-1 float-left w-100 p-0 font-weight-light">Últimos posts</p>
    <ul class="text-left float-left w-100 my-2 p-0 list-unstyled">
        <?php 
        // the query to set the posts per page to 3
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array('posts_per_page' => 5, 'paged' => $paged );
        query_posts($args); ?>
        <!-- the loop -->
        <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
            <li class="mb-3 mb-md-2 color"><a href="<?php the_permalink(''); ?>" alt="Leia '<?php the_title(''); ?>'"><?php the_title(''); ?></a></li>
        <?php endwhile; ?>
        <?php else : ?>
        <!-- No posts found -->
        <?php endif; wp_reset_postdata(); wp_reset_query();?>  

        <a href="<?php bloginfo('url'); ?>/historias-de-sucesso" class="btn btn-outline blue float-right my-1 my-md-2 btn-sm">Ver todos</a>
    </ul>
    <p class="h4 my-1 float-left w-100 p-0 font-weight-light">Categorias</p>
    <ul class="text-left float-left w-100 my-2 p-0 list-unstyled">
    <li class="mb-3 mb-md-2 color"><a href="<?php bloginfo('url'); ?>/categoria/marketing">Marketing</a></li>
    <li class="mb-3 mb-md-2 color"><a href="<?php bloginfo('url'); ?>/categoria/gestao">Gestão</a></li>
    <li class="mb-3 mb-md-2 color"><a href="<?php bloginfo('url'); ?>/categoria/sucesso-do-cliente">Sucesso do Cliente</a></li>
    <li class="mb-3 mb-md-2 color"><a href="<?php bloginfo('url'); ?>/categoria/vendas">Vendas</a></li>
    <li class="mb-3 mb-md-2 color"><a href="<?php bloginfo('url'); ?>/materiais-para-baixar">Materiais para baixar</a></li>
    </ul>
    <div class="w-100 row px-0 float-left mt-1 mb-2">
        <a href="<?php bloginfo( 'url' ); ?>" alt="Ir para a capa"  class="col-4 col-md-7 col-xl-5 mx-auto mb-1" >
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet.png" alt="SimplesVet - Gestão pet suu" class="logo" />
        </a> 
        <div class="w-100 float-left"></div>
        <button type="submit" class="btn btn-primary btn-sm bg blue text-white btn-sm mb-3 mb-md-0 mx-auto mt-3">
            Experimente SimplesVet
        </button>             
    </div>   
</nav>