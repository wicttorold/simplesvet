<div class="col-12 col-md-6 float-left post">
    <a href="<?php the_permalink(''); ?>">
        <div style="background: url('<?php echo get_thumb_share( get_post_thumbnail_id( $post->ID )); ?>') center center no-repeat; background-size: cover; min-height: 40vh" class="rounded w-100 p-5">
        </div>
    </a>
    <div class="w-100 rounded-bottom px-0 py-2">
        <div <?php post_class('w-100 categoryLink mb-1'); ?>>
            <?php the_category( ' ,'); ?>
        </div>
        <div class="w-100 float-left">
        </div>
        <a href="<?php the_permalink(''); ?>" class="w-75 float-left postTitle mb-3 title">
            <?php the_title( ''); ?>
        </a>
    </div>
</div> 