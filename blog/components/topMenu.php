<?php echo get_template_part( 'components/fixedMenu'); ?>
<div class="w-100">
    <header class="container">
        <div class="row py-4 mt-2 mt-md-2">
        <div class="col-8 col-sm-6 col-md-6 float-left">
            <div class="row align-items-end">
                <a href="<?php bloginfo( 'url' ); ?>" alt="Ir para a capa"  class="col-10 col-lg-6 float-left" >
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simplesvet.png" alt="SimplesVet" class="logo" />
                </a>
                <p class="col-10 col-lg-6 float-left text-right text-lg-left mb-2 sitename"><?php bloginfo('description');?></p>
            </div>
        </div>
        <div class="col-2 offset-2 offset-sm-4 offset-md-0 col-md-6 float-right text-right pt-0 pt-md-2" id="menu">
            <small><i class="fas fa-bars fa-2x menucall pt-1"></i></small>
        </div>         
        </div>
    </header>
</div>